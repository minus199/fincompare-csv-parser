package entities

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"crypto/md5"
	"time"
)

type IUser interface {
	JSONable
	Hash() ([16]byte, error)
}

type User struct {
	FullName     string    `xorm:"varchar(80) not null"`
	EmailAddress string    `xorm:"varchar(250) not null unique 'user_email_id'"`
	CreatedAt    time.Time `xorm:"created"`
	UpdatedAt    time.Time `xorm:"updated"`
}

func (u *User) ToJson() ([]byte, error) {
	return json.Marshal(u)
}

func (u *User) Hash() ([16]byte, error) {
	x, e := u.ToJson()
	if e != nil {
		x = make([]byte, 0)
	}
	return md5.Sum(x), e
}

func NewUser(fullName, email string) *User {
	return &User{FullName: fullName, EmailAddress: email}
}

func NewUserFromRaw(delivery amqp.Delivery) (*User, error) {
	user := NewUser("", "") // empty by default
	err := json.Unmarshal(delivery.Body, user)
	return user, err
}
