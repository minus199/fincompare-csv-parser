package entities

type JSONable interface {
	ToJson() ([]byte, error)
}