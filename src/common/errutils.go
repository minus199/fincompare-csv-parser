package common

import (
	"github.com/pkg/errors"
	"strings"
)

func WrapAndPanic(err error, msg ...string) {
	werr := errors.Wrap(err, strings.Join(msg, " | "))
	if werr != nil {
		panic(werr)
	}
}
