package common

import (
	"github.com/streadway/amqp"
	"common/entities"
	"github.com/sadlil/gologger"
)

var config = GetConfig()
var logger = gologger.GetLogger(gologger.CONSOLE, gologger.ColoredLog)

func InConnectionContext(queueName string, inContext func(*amqp.Channel, amqp.Queue)) {
	conn, err := amqp.Dial(config.RabbitURI())
	defer conn.Close()
	WrapAndPanic(err, "Failed to connect to RabbitMQ. See static/conf.json for rabbit config. Make sure lib is installed.")

	ch, err := conn.Channel()
	defer ch.Close()
	WrapAndPanic(err, "Failed to open a channel")

	q, err := ch.QueueDeclare(queueName, false, false, false, false, nil)
	WrapAndPanic(err, "Failed to declare a queue")

	inContext(ch, q)
}

func Publish(queueName string, payload entities.JSONable) {
	InConnectionContext(queueName, func(c *amqp.Channel, q amqp.Queue) {
		body, err := payload.ToJson()
		WrapAndPanic(err)

		err = c.Publish("", q.Name, false, false, amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(body),
		})

		WrapAndPanic(err)
	})
}

func Consume(queueName string, handler func(amqp.Delivery)) {
	InConnectionContext(queueName, func(c *amqp.Channel, q amqp.Queue) {
		msgs, err := c.Consume(q.Name, "", true, false, false, false, nil, )
		WrapAndPanic(err, "Failed to register a consumer")

		forever := make(chan bool)

		go func() {
			for d := range msgs {
				handler(d)
			}
		}()

		logger.Info("[*] Waiting. CTRL+C to exit.")
		<-forever
	})
}
