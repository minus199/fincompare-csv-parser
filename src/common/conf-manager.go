package common

import (
	"sync"
	"github.com/tkanos/gonfig"
	"fmt"
)

var configuration Configuration
var once sync.Once

type Configuration struct {
	Workers        map[string]string
	RabbitUName    string
	RabbitPassword string
	RabbitPort     int
	RabbitURL      string
	DBURI          string
}

func (c *Configuration) RabbitURI() string {
	return fmt.Sprintf(c.RabbitURL, c.RabbitUName, c.RabbitPassword, c.RabbitPort)
}

func GetConfig() Configuration {
	once.Do(func() {
		configuration = Configuration{}
		err := gonfig.GetConf("static/conf.json", &configuration)
		WrapAndPanic(err)
	})

	return configuration
}
