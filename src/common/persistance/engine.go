package persistance

import (
	"github.com/go-xorm/xorm"
	_ "github.com/mattn/go-sqlite3"
	"sync"
	"common"
	"common/entities"
)

var once sync.Once
var orm *xorm.Engine
var config = common.GetConfig()

func Engine() *xorm.Engine {
	once.Do(func() {
		_orm, err := xorm.NewEngine("sqlite3", config.DBURI)
		common.WrapAndPanic(err, "Internal DB error")
		orm = _orm

		u := new(entities.User)
		err = orm.Sync(u)
		common.WrapAndPanic(err, "failed to connect database")
	})

	return orm
}
