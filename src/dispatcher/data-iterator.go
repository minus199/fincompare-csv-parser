package main

import (
	"io"
	"encoding/csv"
	"os"
	//"../common"
	//_ "../common/entities"

	"common"
	"fmt"
	"common/entities"
)

func NewIterator(path string) *DataIterator {
	descriptor, err := os.Open(path)
	common.WrapAndPanic(err, "Unable to load data file")

	iter := &DataIterator{csvReader: csv.NewReader(descriptor), dataChannel: make(chan []string)}
	return iter
}

func NewPublishingIterator(srcPath string, handler func([]string) entities.JSONable) {
	iter := NewIterator(srcPath)
	logger.Info(fmt.Sprintf("Got payload from %s. Starting dispatch", srcPath))

	for record := range iter.Iterate() {
		logger.Info(fmt.Sprintf("Dispatching record: %v", record))
		common.Publish("raw-users-data-queue", handler(record))
	}
}

type DataIterator struct {
	csvReader   *csv.Reader
	dataChannel chan []string
}

func (iter *DataIterator) Next(cb func([]string)) error {
	record, err := iter.csvReader.Read()
	if err == io.EOF {
		return io.EOF
	}

	if err != nil {
		return err
	}

	cb(record)
	return nil
}

func (iter *DataIterator) Iterate() <-chan []string {
	go func() {
		for {
			if stop := iter.Next(func(record []string) {
				iter.dataChannel <- record
			}); stop == io.EOF {
				// An exit condition could be put here. Otherwise, will continue to listen for file changes
				//close(iter.dataChannel)
				//break
			}
		}
	}()

	return iter.dataChannel
}
