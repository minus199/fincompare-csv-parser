package main

import (
	"github.com/sadlil/gologger"
	"os"
	"common/entities"
	"fmt"
)

var logger = gologger.GetLogger(gologger.CONSOLE, gologger.ColoredLog)

func extractUserArgs() string {
	args := os.Args

	if len(args) > 1 {
		return args[1]
	}

	srcFile := "./static/data.csv"
	logger.Warn(fmt.Sprintf("insufficient arguments(specify src path of data files). Using %s", srcFile))
	return srcFile
}

func main() {
	NewPublishingIterator(extractUserArgs(), func(record []string) entities.JSONable {
		return entities.NewUser(record[0], record[1])
	})
}
