package main

import (
	"github.com/streadway/amqp"
	"fmt"

	"common"
	"common/entities"
	"github.com/sadlil/gologger"
	"common/persistance"
	"strings"
)

var logger = gologger.GetLogger(gologger.CONSOLE, gologger.ColoredLog)

func main() {
	logger.Info("Consuming data...")
	orm := persistance.Engine()

	common.Consume("raw-users-data-queue", func(delivery amqp.Delivery) {
		user, err := entities.NewUserFromRaw(delivery)
		if err != nil {
			logger.Error(fmt.Sprintf("Got error while consuming: %v", err))
		} else {
			logger.Info(fmt.Sprintf("Persisting User %s with email %s", user.FullName, user.EmailAddress))
			_, err := orm.InsertOne(user)

			if err != nil && !strings.HasPrefix(err.Error(), "UNIQUE constraint failed") {
				common.WrapAndPanic(err)
			}
		}
	})
}
