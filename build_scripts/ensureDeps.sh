#! /bin/bash
#
# installDeps.sh
# Copyright (C) 2018 minus <minus@MiNuS-Main>
#
# Distributed under terms of the MIT license.
#

export GOPATH=$(pwd)
while read -u 10 pkg; do
  if [[ $pkg = *[!\ ]* ]]; then
    echo Installing $pkg 
    go get $pkg
  fi
done 10<./build_scripts/deps.txt
