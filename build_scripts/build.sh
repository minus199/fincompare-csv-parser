#! /bin/bash
#
# start.sh
# Copyright (C) 2018 minus <minus@MiNuS-Main>
#
# Distributed under terms of the MIT license.
#

BASE_DIR=$(pwd)
export GOPATH=${BASE_DIR}

rm -rf $BASE_DIR/dist && mkdir $_

for folder in $BASE_DIR/src/{dispatcher,workers}; do 
  echo Building $folder...
  [ -d "$folder" ] && cd "$folder" && go clean && go build -o "$(basename $folder).bin"
done

echo Building common...
cd $BASE_DIR/src/common && go build && go install && cd $BASE_DIR

# file linking
mv $BASE_DIR/src/{dis,work}*/*.bin $BASE_DIR/dist
cd $BASE_DIR/dist && ln -sf $BASE_DIR/static ./static && cd $BASE_DIR

echo All libs were built. Binaries are $(ls $BASE_DIR/dist/*.bin)



